<?php

namespace Drupal\entity_update_diff_json\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class of EntityUpdateDiffJsonController.
 *
 * @package Drupal\entity_update_diff_json\Controller
 */
class EntityUpdateDiffJsonController extends ControllerBase {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new DatabaseLockBackend.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }
  
  /**
   * The table of entity update json diff.
   *
   * @return array
   *   A renderable array of table columns.
   */
  public function list() {
    $entity_rows = $this->getEntityUpdateDiffJsonRows();

    $header = [
      'entity_title' => $this->t('Entity Title'),
      'user_name' => $this->t('Username'),
      'created' => $this->t('Created'),
      'entity_id' => $this->t('Entity ID'),
      'entity_type' => $this->t('Entity Type'),
      'original' => $this->t('Original'),
      'updated' => $this->t('Updated'),
      'differences' => $this->t('Differences'),
    ];

    $rows = [];
    foreach ($entity_rows as $value) {
      $rows[] = [
        $value->entity_title,
        $value->user_name,
        $value->created,
        $value->entity_id,
        $value->entity_type,
        $value->original,
        $value->updated,
        $value->differences,
      ];
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

  /**
   * Query to get entity update diff json results.
   *
   * @return array
   *   A renderable array of results.
   */
  public function getEntityUpdateDiffJsonRows() {
    $database = $this->database;
    $query = $database->select('entity_update_diff_json', 'eudj');
    $query->fields('eudj');
    $query->OrderBy('created', 'DESC');
    $results = $query->execute()->fetchAll();

    return $results;
  }

}
