<?php

namespace Drupal\entity_update_diff_json\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form for the configuration settings.
 */
class EntityUpdateDiffJsonForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_update_diff_json_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_update_diff_json_form.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_update_diff_json_form.settings');

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => ('Entity Types:'),
      '#options' => $this->getEntityTypes(),
      '#options' => [
        'commerce_product_variation' => $this->t('Product variation'),
        'commerce_product' => $this->t('Product'),
        'node' => $this->t('Content'),
      ],
      '#description' => $this->t('Select entity types who will be stored in database'),
      '#default_value' => $config->get('entity_types'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Select'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Returns an array of relevant entity types.
   *
   * @return array
   *   An array of entity types.
   */
  private function getEntityTypes() {
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_definitions = $entity_type_manager->getDefinitions();

    $entity_types_list = [];
    foreach ($entity_definitions as $entity_name => $entity_definition) {
      $entity_types_list[$entity_name] = (string) $entity_definition->getLabel();
    }

    return $entity_types_list;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('entity_update_diff_json_form.settings');
    $config->set('entity_types', $form_state->getValue('entity_types'))->save();
    $config->save();
  }

}
