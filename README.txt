Entity update diff json

This module allows you to store in the database in json format the updated and
previous version of the entities selected in the backoffice.

You will be able to select who can see the list with the changes as well as the
entities to be stored.
